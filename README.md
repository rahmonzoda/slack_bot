# set env
``` sh
mkvirtualenv --python=python3 slack
```

# api make sure you set env variables for local dev
``` sh
export SLACK_BOT_TOKEN='token***'
```

# Install pip dependencies
``` sh
pip install -r requirements.txt
```

# run local
``` sh
python bot.py
```

# bot commands
``` sh
@bot_name all
@bot_name staging
@bot_name production
```
