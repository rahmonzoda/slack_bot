import os
import re
import time
from subprocess import call

from slackclient import SlackClient

# instantiate Slack client
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
COMMANDS = ['all', 'staging', 'production']
MENTION_REGEX = '^<@(|[WU].+?)>(.*)'


def build(channel, isProd=False, branch=None):
    text = 'Staging building...'
    build_dir = '/hostaway-website-staging'

    if isProd:
        text = 'Production building...'
        build_dir = '/hostaway-website'

    slack_client.api_call(
        'chat.postMessage',
        channel=channel,
        text=text
    )
    os.chdir(build_dir)
    call('yarn build')
    slack_client.api_call(
        'chat.postMessage',
        channel=channel,
        text='Building successful {}'.format(build_dir)
    )


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack API to find bot commands.
        If a bot command is found, this function returns a tuple of command
        and channel. If its not found, then returns None, None.
    """
    for event in slack_events:
        if event['type'] == 'message' and 'subtype' not in event:
            user_id, message = parse_direct_mention(event['text'])
            if user_id == starterbot_id:
                return message, event['channel']
    return None, None


def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in
        message text and returns the user ID which was mentioned. If there is
        no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    """
        The first group contains the username, the second group contains
        the remaining message
    """
    return (
        matches.group(1),
        matches.group(2).strip()
    ) if matches else (None, None)


def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """

    if command in COMMANDS:
        slack_client.api_call(
            'chat.postMessage',
            channel=channel,
            text='Sure...please, wait few minutes!'
        )

    if command == 'all':
        # staging
        build(channel)
        # production
        build(channel, True)
    elif command == 'staging':
        # staging
        build(channel)
    elif command == 'production':
        # production
        build(channel, True)
    else:
        slack_client.api_call(
            'chat.postMessage',
            channel=channel,
            text='Not sure what you mean. Try *staging*'
        )


if __name__ == '__main__':
    if slack_client.rtm_connect(with_team_state=False):
        print('Starter Bot connected and running!')
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call('auth.test')['user_id']
        while True:
            command, channel = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel)
            time.sleep(1)
    else:
        print('Connection failed. Exception traceback printed above.')
